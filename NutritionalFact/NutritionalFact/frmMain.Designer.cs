﻿namespace NutritionalFact
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.lstFoods = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtCalories = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFat = new System.Windows.Forms.TextBox();
            this.txtSodium = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCarbs = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSugar = new System.Windows.Forms.TextBox();
            this.Sugar = new System.Windows.Forms.Label();
            this.txtFiber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtProtein = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtServing = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPer = new System.Windows.Forms.TextBox();
            this.cmbUnits = new System.Windows.Forms.ComboBox();
            this.cmbFat = new System.Windows.Forms.ComboBox();
            this.cmbSodium = new System.Windows.Forms.ComboBox();
            this.cmbCarbs = new System.Windows.Forms.ComboBox();
            this.cmbSugar = new System.Windows.Forms.ComboBox();
            this.cmbFibers = new System.Windows.Forms.ComboBox();
            this.cmbProtein = new System.Windows.Forms.ComboBox();
            this.txtFood = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAdded = new System.Windows.Forms.TextBox();
            this.rtxtBOutput = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstFoods
            // 
            this.lstFoods.FormattingEnabled = true;
            this.lstFoods.Location = new System.Drawing.Point(346, 57);
            this.lstFoods.Name = "lstFoods";
            this.lstFoods.Size = new System.Drawing.Size(688, 251);
            this.lstFoods.TabIndex = 999;
            this.lstFoods.SelectedIndexChanged += new System.EventHandler(this.lstFoods_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1034, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // txtCalories
            // 
            this.txtCalories.Location = new System.Drawing.Point(97, 105);
            this.txtCalories.Name = "txtCalories";
            this.txtCalories.Size = new System.Drawing.Size(100, 20);
            this.txtCalories.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Calories";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Fat";
            // 
            // txtFat
            // 
            this.txtFat.Location = new System.Drawing.Point(97, 131);
            this.txtFat.Name = "txtFat";
            this.txtFat.Size = new System.Drawing.Size(54, 20);
            this.txtFat.TabIndex = 5;
            // 
            // txtSodium
            // 
            this.txtSodium.Location = new System.Drawing.Point(97, 157);
            this.txtSodium.Name = "txtSodium";
            this.txtSodium.Size = new System.Drawing.Size(54, 20);
            this.txtSodium.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Sodium";
            // 
            // txtCarbs
            // 
            this.txtCarbs.Location = new System.Drawing.Point(97, 183);
            this.txtCarbs.Name = "txtCarbs";
            this.txtCarbs.Size = new System.Drawing.Size(54, 20);
            this.txtCarbs.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Carbohydrates";
            // 
            // txtSugar
            // 
            this.txtSugar.Location = new System.Drawing.Point(97, 209);
            this.txtSugar.Name = "txtSugar";
            this.txtSugar.Size = new System.Drawing.Size(54, 20);
            this.txtSugar.TabIndex = 11;
            // 
            // Sugar
            // 
            this.Sugar.AutoSize = true;
            this.Sugar.Location = new System.Drawing.Point(36, 209);
            this.Sugar.Name = "Sugar";
            this.Sugar.Size = new System.Drawing.Size(35, 13);
            this.Sugar.TabIndex = 10;
            this.Sugar.Text = "Sugar";
            // 
            // txtFiber
            // 
            this.txtFiber.Location = new System.Drawing.Point(97, 235);
            this.txtFiber.Name = "txtFiber";
            this.txtFiber.Size = new System.Drawing.Size(54, 20);
            this.txtFiber.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 235);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Fibers";
            // 
            // txtProtein
            // 
            this.txtProtein.Location = new System.Drawing.Point(97, 261);
            this.txtProtein.Name = "txtProtein";
            this.txtProtein.Size = new System.Drawing.Size(54, 20);
            this.txtProtein.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 264);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Protein";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(122, 287);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 16;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtServing
            // 
            this.txtServing.Location = new System.Drawing.Point(284, 57);
            this.txtServing.Name = "txtServing";
            this.txtServing.Size = new System.Drawing.Size(56, 20);
            this.txtServing.TabIndex = 17;
            this.txtServing.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(212, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Serving Size";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(65, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Per";
            // 
            // txtPer
            // 
            this.txtPer.Location = new System.Drawing.Point(97, 80);
            this.txtPer.Name = "txtPer";
            this.txtPer.Size = new System.Drawing.Size(41, 20);
            this.txtPer.TabIndex = 0;
            // 
            // cmbUnits
            // 
            this.cmbUnits.FormattingEnabled = true;
            this.cmbUnits.Items.AddRange(new object[] {
            "tbls",
            "cups",
            "tps",
            "ml",
            "g"});
            this.cmbUnits.Location = new System.Drawing.Point(144, 80);
            this.cmbUnits.Name = "cmbUnits";
            this.cmbUnits.Size = new System.Drawing.Size(53, 21);
            this.cmbUnits.TabIndex = 1;
            // 
            // cmbFat
            // 
            this.cmbFat.FormattingEnabled = true;
            this.cmbFat.Items.AddRange(new object[] {
            "tbls",
            "tps",
            "ml",
            "g"});
            this.cmbFat.Location = new System.Drawing.Point(157, 131);
            this.cmbFat.Name = "cmbFat";
            this.cmbFat.Size = new System.Drawing.Size(40, 21);
            this.cmbFat.TabIndex = 1001;
            // 
            // cmbSodium
            // 
            this.cmbSodium.FormattingEnabled = true;
            this.cmbSodium.Items.AddRange(new object[] {
            "tbls",
            "tps",
            "ml",
            "g",
            "mg"});
            this.cmbSodium.Location = new System.Drawing.Point(157, 157);
            this.cmbSodium.Name = "cmbSodium";
            this.cmbSodium.Size = new System.Drawing.Size(40, 21);
            this.cmbSodium.TabIndex = 1002;
            // 
            // cmbCarbs
            // 
            this.cmbCarbs.FormattingEnabled = true;
            this.cmbCarbs.Items.AddRange(new object[] {
            "tbls",
            "tps",
            "ml",
            "g"});
            this.cmbCarbs.Location = new System.Drawing.Point(157, 183);
            this.cmbCarbs.Name = "cmbCarbs";
            this.cmbCarbs.Size = new System.Drawing.Size(40, 21);
            this.cmbCarbs.TabIndex = 1003;
            // 
            // cmbSugar
            // 
            this.cmbSugar.FormattingEnabled = true;
            this.cmbSugar.Items.AddRange(new object[] {
            "tbls",
            "tps",
            "ml",
            "g"});
            this.cmbSugar.Location = new System.Drawing.Point(157, 209);
            this.cmbSugar.Name = "cmbSugar";
            this.cmbSugar.Size = new System.Drawing.Size(40, 21);
            this.cmbSugar.TabIndex = 1004;
            // 
            // cmbFibers
            // 
            this.cmbFibers.FormattingEnabled = true;
            this.cmbFibers.Items.AddRange(new object[] {
            "tbls",
            "tps",
            "ml",
            "g"});
            this.cmbFibers.Location = new System.Drawing.Point(157, 234);
            this.cmbFibers.Name = "cmbFibers";
            this.cmbFibers.Size = new System.Drawing.Size(40, 21);
            this.cmbFibers.TabIndex = 1005;
            // 
            // cmbProtein
            // 
            this.cmbProtein.FormattingEnabled = true;
            this.cmbProtein.Items.AddRange(new object[] {
            "tbls",
            "tps",
            "ml",
            "g"});
            this.cmbProtein.Location = new System.Drawing.Point(157, 260);
            this.cmbProtein.Name = "cmbProtein";
            this.cmbProtein.Size = new System.Drawing.Size(40, 21);
            this.cmbProtein.TabIndex = 1006;
            // 
            // txtFood
            // 
            this.txtFood.Location = new System.Drawing.Point(54, 57);
            this.txtFood.Name = "txtFood";
            this.txtFood.Size = new System.Drawing.Size(143, 20);
            this.txtFood.TabIndex = 1007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 1008;
            this.label9.Text = "Name";
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(265, 287);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(75, 23);
            this.btnCalculate.TabIndex = 1009;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 295);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 1010;
            this.label10.Text = "Added";
            // 
            // txtAdded
            // 
            this.txtAdded.Location = new System.Drawing.Point(56, 290);
            this.txtAdded.Name = "txtAdded";
            this.txtAdded.Size = new System.Drawing.Size(60, 20);
            this.txtAdded.TabIndex = 1011;
            this.txtAdded.Text = "1";
            // 
            // rtxtBOutput
            // 
            this.rtxtBOutput.Location = new System.Drawing.Point(728, 314);
            this.rtxtBOutput.Name = "rtxtBOutput";
            this.rtxtBOutput.Size = new System.Drawing.Size(294, 143);
            this.rtxtBOutput.TabIndex = 1012;
            this.rtxtBOutput.Text = "";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Meiryo UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(346, 35);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(688, 19);
            this.label11.TabIndex = 1013;
            this.label11.Text = "Ingredients";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Meiryo UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(346, 314);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(378, 19);
            this.label12.TabIndex = 1014;
            this.label12.Text = "Macros/Serving";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 467);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.rtxtBOutput);
            this.Controls.Add(this.txtAdded);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtFood);
            this.Controls.Add(this.cmbProtein);
            this.Controls.Add(this.cmbFibers);
            this.Controls.Add(this.cmbSugar);
            this.Controls.Add(this.cmbCarbs);
            this.Controls.Add(this.cmbSodium);
            this.Controls.Add(this.cmbFat);
            this.Controls.Add(this.cmbUnits);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtPer);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtServing);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtProtein);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtFiber);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtSugar);
            this.Controls.Add(this.Sugar);
            this.Controls.Add(this.txtCarbs);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtSodium);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtFat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCalories);
            this.Controls.Add(this.lstFoods);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "Nutritional Facts";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstFoods;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.TextBox txtCalories;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFat;
        private System.Windows.Forms.TextBox txtSodium;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCarbs;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSugar;
        private System.Windows.Forms.Label Sugar;
        private System.Windows.Forms.TextBox txtFiber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtProtein;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtServing;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPer;
        private System.Windows.Forms.ComboBox cmbUnits;
        private System.Windows.Forms.ComboBox cmbFat;
        private System.Windows.Forms.ComboBox cmbSodium;
        private System.Windows.Forms.ComboBox cmbCarbs;
        private System.Windows.Forms.ComboBox cmbSugar;
        private System.Windows.Forms.ComboBox cmbFibers;
        private System.Windows.Forms.ComboBox cmbProtein;
        private System.Windows.Forms.TextBox txtFood;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtAdded;
        private System.Windows.Forms.RichTextBox rtxtBOutput;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}

