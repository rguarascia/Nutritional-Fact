﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NutritionalFact
{
    public partial class frmMain : Form
    {
        string[,] ingredients = new string[10, 10];
        public frmMain()
        {
            InitializeComponent();
        }

        private void lstFoods_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            lstFoods.Items.Add(txtFood.Text + " | Calories: " + txtCalories.Text + " | Fat: " + txtFat.Text + " " + cmbFat.Text + " | Sodium: " + txtSodium.Text + " " + cmbSodium.Text + " | Carbs: " + txtCarbs.Text + " " + cmbCarbs.Text +
                " | Sugars: " + txtSugar.Text + " " + cmbSugar.Text + " | Fibers: " + txtFiber.Text + " " + cmbFibers.Text + " | Protein: " + txtProtein.Text + " " +  cmbProtein.Text + " | Per: " + txtPer.Text + " " + cmbUnits.Text + " | Added: " + txtAdded.Text);
            txtCalories.Clear();
            txtCarbs.Clear();
            txtFat.Clear();
            txtFiber.Clear();
            txtProtein.Clear();
            txtSodium.Clear();
            txtSugar.Clear();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            cmbUnits.SelectedIndex = 0;
            cmbCarbs.SelectedIndex = 3;
            cmbFat.SelectedIndex = 3;
            cmbFibers.SelectedIndex = 3;
            cmbSodium.SelectedIndex = 4;
            cmbSugar.SelectedIndex = 3;
            cmbProtein.SelectedIndex = 3;
        }

        private double convertToGrams(string val, string currentMeasurment)
        {
            int value = int.Parse(val);
            switch (currentMeasurment)
            {
                case "ml":
                    return value;
                case "mg":
                    return value * 0.0001;
                case "cups":
                    return value * 340;
                case "tbls":
                    return value * (double)14.3;
                case "tps":
                    return value * (long)4.76666666667;
                case "g":
                    return value;
                default:
                    //Custom value. per egg, per carton etc
                    return value;
            }
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            rtxtBOutput.Clear();
            int counter = 0;
            if (lstFoods.Items.Count != 0)
            {
                // every ingredient
                foreach (string x in lstFoods.Items)
                {
                    string[] eachFactor = x.Split('|');
                    //each fact
                    for (int i = 0; i < eachFactor.Length; i++)
                    {
                        //ignore calories
                        if (i > 1)
                        {
                            string[] eachMeasrure = eachFactor[i].Split(':'); //gets each value
                            //1 is the ammount
                            //2 is the units
                            string[] details = eachMeasrure[1].Split(' ');
                            //makes sure value retrieve, then only gets needed information to convert
                            if (details.Count() <= 2 || i <= 7)
                            {
                                if (i - 2 != 7)
                                    //adds all the ingredients to an 2d array
                                    ingredients[counter, i - 2] = convertToGrams(details[1], details[2]).ToString();
                                else
                                    ingredients[counter, 9] = details[1]; //Sets the upmost array val to added
                            }

                        }
                    }
                    string cals = eachFactor[1].Split(':')[1];
                    ingredients[counter, 8] = cals; //Sets the second to last val to calories
                    counter++;

                }
            }
            else
            {
                MessageBox.Show("You need to add ingredients before calculations can occur");
            }
            int totalCalories = 0;
            int totalFat = 0;
            double  totalSodium = 0;
            int totalFibers = 0;
            int totalSugars= 0;
            int totalCarbs = 0;
            int totalProtien= 0;
            /*
             * Fat
             * Sodium
             * Carbs
             * Sugar
             * Fibers
             * Protien
             */
            for (int x = 0; x < ingredients.GetUpperBound(0); x++)
            {
                {
                    int added = 0;
                    if(ingredients[x,9] != null)
                        added = int.Parse(ingredients[x, 9]);
                    if (ingredients[x, 0] != null)
                        totalFat += int.Parse(ingredients[x, 0]) * added;
                    if (ingredients[x, 1] != null)
                        totalSodium += double.Parse(ingredients[x, 1]) * added;
                    if (ingredients[x, 2] != null)
                        totalCarbs += int.Parse(ingredients[x, 2]) * added;
                    if (ingredients[x, 3] != null)
                        totalSugars += int.Parse(ingredients[x, 3]) * added;
                    if (ingredients[x, 4] != null)
                        totalFibers += int.Parse(ingredients[x, 4]) * added;
                    if (ingredients[x, 5] != null)
                        totalProtien += int.Parse(ingredients[x, 5]) * added;
                    if (ingredients[x, 8] != null)
                        totalCalories += int.Parse(ingredients[x, 8]) * added;
                }
            }
            int serving = int.Parse(txtServing.Text);
            totalFat /= serving;
            totalSodium /= serving;
            totalCarbs /= serving;
            totalSugars /= serving;
            totalFibers /= serving;
            totalProtien /= serving;
            totalCalories /= serving;
            rtxtBOutput.AppendText("Nutrition information");
            rtxtBOutput.AppendText(Environment.NewLine);
            rtxtBOutput.AppendText("Calories: " + totalCalories);
            rtxtBOutput.AppendText(Environment.NewLine);
            rtxtBOutput.AppendText("Fat: " + totalFat);
            rtxtBOutput.AppendText(Environment.NewLine);
            rtxtBOutput.AppendText("Sodium: " + totalSodium);
            rtxtBOutput.AppendText(Environment.NewLine);
            rtxtBOutput.AppendText("Carbohydrate: " + totalCarbs);
            rtxtBOutput.AppendText(Environment.NewLine);
            rtxtBOutput.AppendText("\t Sugars: " + totalSugars);
            rtxtBOutput.AppendText(Environment.NewLine);
            rtxtBOutput.AppendText("\t Fibers: " + totalFibers);
            rtxtBOutput.AppendText(Environment.NewLine);
            rtxtBOutput.AppendText("Protien: " + totalProtien);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lstFoods.Items.Count != 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "nutr files (*.nutr)|*.nutr|All files (*.*)|*.*";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter writer = new StreamWriter(sfd.FileName);
                    foreach (string l in lstFoods.Items)
                    {
                        writer.WriteLine(l);
                    }
                    writer.Flush();
                    writer.Close();
                }
            } else
            {
                MessageBox.Show("You must add ingretients before you can save to a file");
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "nutr files (*.nutr)|*.nutr|All files (*.*)|*.*";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string line;
                System.IO.StreamReader sr = new
                   System.IO.StreamReader(ofd.FileName);
                while((line = sr.ReadLine()) != null)
                {
                    lstFoods.Items.Add(line);
                }
                sr.Close();
            }
        }

        private string perServing(string val, string serving)
        {
            /* TODO:
             * Calculate per serving. 
             * Example per 2 tablespoons but only add 3, must convert macros to per 1 then times 3
             */
            return "";
        }
    }
}
